require grafana.inc

# --> armv7
SRC_URI:armv7a = "https://dl.grafana.com/oss/release/${BPN}-${PV}.linux-armv7.tar.gz;\
sha256sum=48d2aee1fcc9443bea67f3e504be9cc38689b76db787e588bf337c2efc797b51 \
file://LICENSE"

SRC_URI_einstein = "https://dl.grafana.com/oss/release/${BPN}-${PV}.linux-armv7.tar.gz;\
sha256sum=48d2aee1fcc9443bea67f3e504be9cc38689b76db787e588bf337c2efc797b51 \
file://LICENSE"

SRC_URI_imx6q-phytec-mira-rdk-nand = "https://dl.grafana.com/oss/release/${BPN}-${PV}.linux-armv7.tar.gz;\
sha256sum=48d2aee1fcc9443bea67f3e504be9cc38689b76db787e588bf337c2efc797b51 \
file://LICENSE"

# <-- armv7

# --> amd64
SRC_URI:x86-64 = "https://dl.grafana.com/oss/release/${BPN}-${PV}.linux-amd64.tar.gz;\
sha256sum=860197db2000579589b02fbce724af5d008289efd208637f56ea311bdb726cfa \
file://LICENSE"
# <-- amd64

LIC_FILES_CHKSUM = "file://${WORKDIR}/LICENSE;md5=31f6db4579f7bbf48d02bff8c5c3a6eb"

LICENSE = "Apache-2.0"

#DEPENDS:x86-64 = "zlib fontconfig freetype"

UNPACKDIR = "${WORKDIR}/"
S = "${UNPACKDIR}/${BPN}-v${PV}"
#INSANE_SKIP = "32bit-time"
#INSANE_SKIP = "already-stripped"
