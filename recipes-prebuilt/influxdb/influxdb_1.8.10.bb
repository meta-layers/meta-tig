require influxdb.inc

# --> armhf
SRC_URI:armv7a = "https://dl.influxdata.com/influxdb/releases/influxdb-${PV}_linux_armhf.tar.gz;\
sha256sum=33daa5312a24b82365b98387fd330c574c990260a836fa810604cadc085050c5 \
file://LICENSE"

SRC_URI_einstein = "https://dl.influxdata.com/influxdb/releases/influxdb-${PV}_linux_armhf.tar.gz;\
sha256sum=33daa5312a24b82365b98387fd330c574c990260a836fa810604cadc085050c5 \
file://LICENSE"
# <-- armhf

# --> amd64
SRC_URI:x86-64 = "https://dl.influxdata.com/influxdb/releases/influxdb-${PV}-static_linux_amd64.tar.gz;\
sha256sum=084313d45052afdcdbb04da807c23c3fa6ed5f0ae3a2956ea70d60a14d1622ee \
file://LICENSE"
# <-- amd64

LIC_FILES_CHKSUM = "file://${WORKDIR}/LICENSE;md5=f39a8d10930fb37bd59adabb3b9d0bd6"

# The open source license for InfluxDB is available in the GitHub repository.
# https://github.com/influxdata/influxdb/blob/1.8/LICENSE
# I copied LICENSE from there to my meta data
LICENSE = "MIT"

S = "${WORKDIR}/${PN}-${PV}-1"
UNPACKDIR = "${WORKDIR}/"
