# telegraf version and machine specific changes:
FILESEXTRAPATHS:prepend := "${THISDIR}/${PV}/${MACHINE}:"

SRC_URI:append:imx6ul-phytec-segin = " \
         file://influxdb.conf \
"

SRC_URI:append:stm32mp157c-dk2 = " \
         file://influxdb.conf \
"

SRC_URI:append:einstein = " \
         file://influxdb.conf \
"

SRC_URI:append:beagle-bone-black = " \
         file://influxdb.conf \
"

SRC_URI:append:phycore-stm32mp1-2 = " \
         file://influxdb.conf \
"

SRC_URI:append:imx6q-phytec-mira-rdk-nand = " \
         file://influxdb.conf \
"

SRC_URI:append:de0-nano-soc-kit = " \
         file://influxdb.conf \
"

do_install:append () {
if [ -f ${UNPACKDIR}/influxdb.conf ]; then
   cp ${UNPACKDIR}/influxdb.conf ${D}${sysconfdir}/influxdb/

   # overwrite config file with custom config file, if it exists
   if [ -f ${UNPACKDIR}/${influxdb-cfg-file} ]; then
      install -m 0644 ${UNPACKDIR}/${influxdb-cfg-file} ${D}${sysconfdir}/influxdb/influxdb.conf
   fi
fi
}

### temporarily only for license experiments begin
# --> license detector - does not work - needs pkgs
# do_devshell[depends] += "github.com-google-go-license-detector-native:do_populate_sysroot"
# <-- license detector
### temporarily only for license experiments end




