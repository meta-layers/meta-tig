require telegraf.inc

# --> armhf
SRC_URI:armv7a = "https://dl.influxdata.com/telegraf/releases/${BPN}-${PV}_linux_armhf.tar.gz;\
sha256sum=dc459fcceed3f9bf25a798c45acb6ce3896fbccd155b9e57d66847cc56aac9f4 \
file://LICENSE"

SRC_URI:einstein = "https://dl.influxdata.com/telegraf/releases/${BPN}-${PV}_linux_armhf.tar.gz;\
sha256sum=dc459fcceed3f9bf25a798c45acb6ce3896fbccd155b9e57d66847cc56aac9f4 \
file://LICENSE"

SRC_URI:phycore-stm32mp1-2 = "https://dl.influxdata.com/telegraf/releases/${BPN}-${PV}_linux_armhf.tar.gz;\
sha256sum=dc459fcceed3f9bf25a798c45acb6ce3896fbccd155b9e57d66847cc56aac9f4 \
file://LICENSE"

SRC_URI:beagle-bone-black-custom = "https://dl.influxdata.com/telegraf/releases/${BPN}-${PV}_linux_armhf.tar.gz;\
sha256sum=dc459fcceed3f9bf25a798c45acb6ce3896fbccd155b9e57d66847cc56aac9f4 \
file://LICENSE"
# <-- armhf

# --> amd64
SRC_URI:x86-64 = "https://dl.influxdata.com/telegraf/releases/${BPN}-${PV}_linux_amd64.tar.gz;\
sha256sum=8a2055e033c0c2c2dffdc3a9ce62f183a94a08517535fd54263909b53ff9e89d \
file://${UNPACKDIR}/LICENSE"
# <-- amd64

LIC_FILES_CHKSUM = "file://${UNPACKDIR}/LICENSE;md5=c5d3aeddd4f7a4c4993bbdc4a41aec44"

LICENSE = "MIT"

#S = "${WORKDIR}/${PN}-${PV}"
#S = "${WORKDIR}/sources/${PN}-${PV}"
#UNPACKDIR = "${S}"
